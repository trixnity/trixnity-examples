import org.gradle.api.JavaVersion

object Versions {
    val kotlinJvmTarget = JavaVersion.VERSION_17
    const val trixnity = "4.3.3" // https://gitlab.com/trixnity/trixnity/-/releases
    const val kotlinxCoroutines = "1.8.0" // https://github.com/Kotlin/kotlinx.coroutines/releases
    const val kotlinxDatetime = "0.5.0" // https://github.com/Kotlin/kotlinx-datetime/releases
    const val ktor = "2.3.10" // https://github.com/ktorio/ktor/releases
    const val h2 = "2.2.224" // https://github.com/h2database/h2database/releases
    const val copyWebpackPlugin = "12.0.2" // https://github.com/webpack-contrib/copy-webpack-plugin/releases

    const val kotlinLogging = "6.0.9" // https://github.com/MicroUtils/kotlin-logging/releases
    const val logback = "1.5.6" // https://github.com/qos-ch/logback/tags
}