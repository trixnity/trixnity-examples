allprojects {
    repositories {
        mavenCentral()
        google()
        mavenLocal()
        maven("https://oss.sonatype.org/content/repositories/snapshots")
    }
}