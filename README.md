# Trixnity examples

This repository contains some Kotlin Multiplatform examples for [Trixnity](https://gitlab.com/trixnity/trixnity).

## Projects

* [client-ping](./client-ping): ping bot using trixnity-client
* [clientserverapi-client-ping](./clientserverapi-client-ping): ping bot using trixnity-clientserverapi-client

## Run

Use the following gradle tasks to start the examples:

* `:<projectName>:jvmRun` to run on JVM
* `:<projectName>:jsBrowserRun` to run in your browser
* `:<projectName>:runDebugExecutableNative` to run on you host directly

## Local Matrix server for testing

You can run `docker-compose up` within [localinfra directory](./localInfra) to start a local matrix server.