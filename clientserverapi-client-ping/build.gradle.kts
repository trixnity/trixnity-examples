plugins {
    kotlin("multiplatform")
}

kotlin {
    jvmToolchain(JavaLanguageVersion.of(Versions.kotlinJvmTarget.majorVersion).asInt())
    @Suppress("OPT_IN_USAGE")
    jvm {
        compilations.all {
            kotlinOptions.jvmTarget = Versions.kotlinJvmTarget.toString()
        }
    }.mainRun {
        mainClass.set("MainKt")
    }
    js(IR) {
        browser {
            commonWebpackConfig {
                configDirectory = rootDir.resolve("webpack.config.d")
            }
        }
        nodejs()
        binaries.executable()
    }
    listOf(macosArm64(), macosX64(), linuxX64(), mingwX64()).forEach {
        it.apply {
            binaries {
                executable()
            }
        }
    }

    sourceSets {
        all {
            languageSettings.optIn("kotlin.RequiresOptIn")
        }
        commonMain {
            dependencies {
                implementation("net.folivo:trixnity-clientserverapi-client:${Versions.trixnity}")
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:${Versions.kotlinxCoroutines}")
                implementation("org.jetbrains.kotlinx:kotlinx-datetime:${Versions.kotlinxDatetime}")
            }
        }
        jvmMain {
            dependencies {
                implementation("io.ktor:ktor-client-java:${Versions.ktor}")
                implementation("ch.qos.logback:logback-classic:${Versions.logback}")
            }
        }
        jsMain {
            dependencies {
                implementation("io.ktor:ktor-client-js:${Versions.ktor}")
            }
        }
        nativeMain {
            dependencies {
                implementation("io.ktor:ktor-client-curl:${Versions.ktor}")
            }
        }
    }
}