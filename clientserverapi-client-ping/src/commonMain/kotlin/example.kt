import io.ktor.http.*
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import net.folivo.trixnity.clientserverapi.client.MatrixClientServerApiClientImpl
import net.folivo.trixnity.clientserverapi.client.getStateEvent
import net.folivo.trixnity.clientserverapi.model.media.Media
import net.folivo.trixnity.clientserverapi.model.media.ThumbnailResizingMethod
import net.folivo.trixnity.core.model.RoomId
import net.folivo.trixnity.core.model.events.ClientEvent
import net.folivo.trixnity.core.model.events.m.room.ImageInfo
import net.folivo.trixnity.core.model.events.m.room.MemberEventContent
import net.folivo.trixnity.core.model.events.m.room.RoomMessageEventContent.FileBased.Image
import net.folivo.trixnity.core.model.events.m.room.RoomMessageEventContent.TextBased.Text
import net.folivo.trixnity.core.subscribeEvent

suspend fun example() = coroutineScope {
    val matrixRestClient =
        MatrixClientServerApiClientImpl(
            baseUrl = Url("http://localhost:8008"),
        ).apply { accessToken.value = "token" }
    val roomId = RoomId("!room:server")

    val startTime = Clock.System.now()

    matrixRestClient.sync.subscribeEvent<Text, ClientEvent.RoomEvent<Text>> { event ->
        if (event.roomId == roomId) {
            if (Instant.fromEpochMilliseconds(event.originTimestamp) > startTime) {
                val body = event.content.body
                when {
                    body.startsWith("ping") -> {
                        matrixRestClient.room.sendMessageEvent(
                            roomId, Text(body = "pong")
                        ).getOrThrow()
                    }

                    body.startsWith("me") -> {
                        val senderAvatar =
                            matrixRestClient.room.getStateEvent<MemberEventContent>(
                                roomId,
                                event.sender.full
                            ).getOrThrow().avatarUrl
                        if (senderAvatar != null) {
                            val senderAvatarDownload = matrixRestClient.media.downloadThumbnail(
                                senderAvatar,
                                64,
                                64,
                                ThumbnailResizingMethod.CROP
                            ).getOrThrow()
                            val contentLength = senderAvatarDownload.contentLength
                            requireNotNull(contentLength)
                            val uploadedUrl = matrixRestClient.media.upload(
                                Media(
                                    senderAvatarDownload.content,
                                    contentLength,
                                    senderAvatarDownload.contentType ?: ContentType.Application.OctetStream,
                                    null
                                )
                            ).getOrThrow().contentUri
                            matrixRestClient.room.sendMessageEvent(
                                roomId, Image(
                                    body = "avatar image of ${event.sender}",
                                    info = ImageInfo(),
                                    url = uploadedUrl
                                )
                            ).getOrThrow()
                        }

                    }
                }
            }
        }
    }

    val batchToken = MutableStateFlow<String?>(null)
    matrixRestClient.sync.start(
        getBatchToken = { batchToken.value },
        setBatchToken = { batchToken.value = it },
        scope = this
    )
}