import net.folivo.trixnity.client.media.MediaStore
import net.folivo.trixnity.client.media.okio.OkioMediaStore
import okio.Path.Companion.toPath

actual suspend fun createMediaStore(path: String): MediaStore = OkioMediaStore(path.toPath())