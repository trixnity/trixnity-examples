import net.folivo.trixnity.client.store.repository.exposed.createExposedRepositoriesModule
import org.jetbrains.exposed.sql.Database
import org.koin.core.module.Module

actual suspend fun createRepositoriesModule(): Module = createExposedRepositoriesModule(
    database = Database.connect("jdbc:h2:./test;DB_CLOSE_DELAY=-1;"),
)