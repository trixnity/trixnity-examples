import net.folivo.trixnity.client.store.repository.createInMemoryRepositoriesModule
import org.koin.core.module.Module

actual suspend fun createRepositoriesModule(): Module = createInMemoryRepositoriesModule()