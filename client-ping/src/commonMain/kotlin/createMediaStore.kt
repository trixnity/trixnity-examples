import net.folivo.trixnity.client.media.MediaStore

expect suspend fun createMediaStore(path: String): MediaStore