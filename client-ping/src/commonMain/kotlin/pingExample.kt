import io.github.oshai.kotlinlogging.KotlinLogging
import io.ktor.http.*
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import net.folivo.trixnity.client.*
import net.folivo.trixnity.client.room.message.react
import net.folivo.trixnity.client.room.message.reply
import net.folivo.trixnity.client.room.message.text
import net.folivo.trixnity.client.room.toFlowList
import net.folivo.trixnity.client.store.roomId
import net.folivo.trixnity.clientserverapi.client.SyncState
import net.folivo.trixnity.clientserverapi.model.authentication.IdentifierType
import net.folivo.trixnity.core.model.RoomId
import net.folivo.trixnity.core.model.events.m.room.Membership
import net.folivo.trixnity.core.model.events.m.room.RoomMessageEventContent
import kotlin.time.Duration.Companion.milliseconds
import kotlin.time.Duration.Companion.seconds

private val log = KotlinLogging.logger {}

@OptIn(FlowPreview::class)
suspend fun pingExample() = coroutineScope {
    val username = "pingbot"
    val password = "password!123"
    val mediaPath = "media/"
    val baseUrl = Url("http://localhost:8008")

    val matrixClient = MatrixClient.fromStore(
        repositoriesModule = createRepositoriesModule(),
        mediaStore = createMediaStore(mediaPath),
    ).getOrThrow()
        ?: MatrixClient.login(
            baseUrl = baseUrl,
            identifier = IdentifierType.User(username),
            password = password,
            repositoriesModule = createRepositoriesModule(),
            mediaStore = createMediaStore(mediaPath),
        ).onFailure {
            log.warn(it) { "could not login" }
        }.getOrNull()
        ?: MatrixClient.loginWith(
            baseUrl = baseUrl,
            repositoriesModule = createRepositoriesModule(),
            mediaStore = createMediaStore(mediaPath),
            getLoginInfo = { it.register(username, password) }
        ).getOrThrow()

    launch {
        matrixClient.room.getAll().flattenValues().collect { rooms ->
            rooms.filter { it.membership == Membership.INVITE }.forEach {
                matrixClient.api.room.joinRoom(it.roomId).getOrNull()
            }
        }
    }

    val timelineRoomId = MutableStateFlow<RoomId?>(null)
    launch {
        matrixClient.syncState.first { it == SyncState.RUNNING }
        matrixClient.room.getTimelineEventsFromNowOn(decryptionTimeout = 5.seconds).collect { timelineEvent ->
            val content = timelineEvent.content?.getOrNull()
            if (content is RoomMessageEventContent.TextBased.Text) {
                val body = content.body
                when {
                    body.lowercase().startsWith("ping") -> {
                        matrixClient.room.sendMessage(timelineEvent.roomId) {
                            react(timelineEvent, "🏓")
                        }
                        matrixClient.room.sendMessage(timelineEvent.roomId) {
                            text("pong")
                            reply(timelineEvent)
                        }
                    }

                    body.lowercase() == "timeline" -> {
                        timelineRoomId.value = timelineEvent.roomId
                        matrixClient.room.sendMessage(timelineEvent.roomId) {
                            text("start printing timeline into console")
                            reply(timelineEvent)
                        }
                    }

                }
            }
        }
    }

    launch {
        timelineRoomId.collectLatest { roomId ->
            if (roomId != null)
                matrixClient.room.getLastTimelineEvents(roomId)
                    .toFlowList(MutableStateFlow(10))
                    .debounce(100.milliseconds)
                    .collectLatest { timelineEvents ->
                        timelineEvents.reversed().forEach { timelineEvent ->
                            val event = timelineEvent.first().event
                            val content = timelineEvent.first().content?.getOrNull()
                            val sender = matrixClient.user.getById(roomId, event.sender).first()?.name
                            when (content) {
                                is RoomMessageEventContent -> println("${sender}: ${content.body}")
                                null -> println("${sender}: not yet decrypted")
                                else -> {
                                }
                            }
                        }
                        println("### END OF TIMELINE ###")
                    }
        }
    }

    matrixClient.startSync()

    log.info { "The ping bot has been started. Just invite $username and send 'ping hello'." }
    log.info { "You can also write 'timeline' to print the timeline of the room." }

    delay(300000)
    matrixClient.stop()
}