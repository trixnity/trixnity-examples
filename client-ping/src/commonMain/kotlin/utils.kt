import net.folivo.trixnity.client.MatrixClient
import net.folivo.trixnity.clientserverapi.client.MatrixClientServerApiClient
import net.folivo.trixnity.clientserverapi.client.UIA
import net.folivo.trixnity.clientserverapi.model.authentication.AccountType
import net.folivo.trixnity.clientserverapi.model.authentication.Register
import net.folivo.trixnity.clientserverapi.model.uia.AuthenticationRequest

suspend fun MatrixClientServerApiClient.register(
    username: String? = null,
    password: String,
): Result<MatrixClient.LoginInfo> {
    val registerStep = authentication.register(
        password = password,
        username = username,
        accountType = AccountType.USER,
    ).getOrThrow()
    require(registerStep is UIA.Step<Register.Response>)
    val registerResult = registerStep.authenticate(AuthenticationRequest.Dummy).getOrThrow()
    require(registerResult is UIA.Success<Register.Response>)
    val (userId, createdDeviceId, accessToken) = registerResult.value
    requireNotNull(createdDeviceId)
    requireNotNull(accessToken)
    return Result.success(MatrixClient.LoginInfo(userId, createdDeviceId, accessToken))
}