import net.folivo.trixnity.client.media.MediaStore
import net.folivo.trixnity.client.media.indexeddb.IndexedDBMediaStore

actual suspend fun createMediaStore(path: String): MediaStore = IndexedDBMediaStore()