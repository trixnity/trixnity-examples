import net.folivo.trixnity.client.store.repository.indexeddb.createIndexedDBRepositoriesModule

actual suspend fun createRepositoriesModule() = createIndexedDBRepositoriesModule()