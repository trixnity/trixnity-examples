plugins {
    kotlin("multiplatform")
}

kotlin {
    jvmToolchain(JavaLanguageVersion.of(Versions.kotlinJvmTarget.majorVersion).asInt())
    @Suppress("OPT_IN_USAGE")
    jvm {
        compilations.all {
            kotlinOptions.jvmTarget = Versions.kotlinJvmTarget.toString()
        }
    }.mainRun {
        mainClass.set("MainKt")
    }
    js(IR) {
        browser {
            commonWebpackConfig {
                configDirectory = rootDir.resolve("webpack.config.d")
            }
        }
        binaries.executable()
    }
    listOf(macosArm64(), macosX64(), linuxX64(), mingwX64()).forEach {
        it.apply {
            binaries {
                executable()
            }
        }
    }

    sourceSets {
        all {
            languageSettings.optIn("kotlin.RequiresOptIn")
        }
        commonMain {
            dependencies {
                implementation("net.folivo:trixnity-client:${Versions.trixnity}")
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:${Versions.kotlinxCoroutines}")
                implementation("org.jetbrains.kotlinx:kotlinx-datetime:${Versions.kotlinxDatetime}")
                implementation("io.github.oshai:kotlin-logging:${Versions.kotlinLogging}")
            }
        }
        jvmMain {
            dependencies {
                implementation("net.folivo:trixnity-client-repository-exposed:${Versions.trixnity}")
                implementation("net.folivo:trixnity-client-media-okio:${Versions.trixnity}")
                implementation("io.ktor:ktor-client-java:${Versions.ktor}")
                implementation("com.h2database:h2:${Versions.h2}")
                implementation("ch.qos.logback:logback-classic:${Versions.logback}")
            }
        }
        jsMain {
            dependencies {
                implementation(npm("copy-webpack-plugin", Versions.copyWebpackPlugin))
                implementation("io.ktor:ktor-client-js:${Versions.ktor}")
                implementation("net.folivo:trixnity-client-repository-indexeddb:${Versions.trixnity}")
                implementation("net.folivo:trixnity-client-media-indexeddb:${Versions.trixnity}")
            }
        }
        nativeMain {
            dependencies {
                implementation("io.ktor:ktor-client-curl:${Versions.ktor}")
                implementation("net.folivo:trixnity-client-media-okio:${Versions.trixnity}")
            }
        }
    }
}